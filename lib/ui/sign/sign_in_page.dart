import 'package:control_game/ui/menu/menu_inferior.dart';
import 'package:control_game/ui/sign/sign_up_page.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignInPage extends StatefulWidget {
  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  bool senha = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
                color: Colors.white,
                padding: EdgeInsets.only(top: 50),
                child: Column(
                  children: <Widget>[
                    Container(
                      width: 240.0,
                      height: 80.0,
                      padding: EdgeInsets.only(right: 0.0),
//              alignment: Alignment(-1, 0),
                      decoration: BoxDecoration(
                          shape: BoxShape.rectangle,
                          image: DecorationImage(
                              image: AssetImage("images/Logo3.png"),
                              fit: BoxFit.fill)),
                    ),
                    Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 10.0),
                        child: TextField(
                          decoration: InputDecoration(
                            suffixIcon: Icon(Icons.mail_outline),
                            hintText: "E-mail",
                          ),
                        )),
                    Padding(
                        padding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                        child: TextField(
                          obscureText: senha,
                          keyboardType: TextInputType.visiblePassword,
                          decoration: InputDecoration(
                            hintText: "Senha",
                            suffixIcon: IconButton(
                                onPressed: () {
                                  setState(() {
                                    senha = !senha;
                                  });
                                },
                                icon: senha == true
                                    ? Icon(Icons.visibility_off)
                                    : Icon(Icons.visibility)),
                          ),
                        )),
                    Container(
                      padding: EdgeInsets.all(15.0),
                      child: MaterialButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => MenusPage()),
                          );
                        },
                        minWidth: 150.0,
                        highlightElevation: 2,
                        splashColor: Colors.black,
                        child: Text('Entrar',
                            style: TextStyle(
                                color: Colors.orangeAccent, fontSize: 20.0)),
                        shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.orange)),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {},
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.lock),
                          Text("Esqueci minha Senha")
                        ],
                      ),
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SignUpPage()),
                          );
                        },
                        child: Container(
                          padding: EdgeInsets.only(top: 45.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Ainda não tem uma conta? "),
                              Text(
                                "Registre-se",
                                style: TextStyle(color: Colors.orangeAccent),
                              )
                            ],
                          ),
                        )),
                    Padding(
                      padding: EdgeInsets.all(20.0),
                      child: Divider(),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text(
                        "OU ENTRE USANDO UMA CONTA",
                        style: TextStyle(fontSize: 14.0, color: Colors.black),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 30.0, left: 10.0, right: 10.0),
                      child: GestureDetector(
                        child: Container(
                          height: 60.0,
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.indigo, width: 1.5),
                            borderRadius: BorderRadius.all(Radius.circular(
                                    10.0) //         <--- border radius here
                                ),
                          ),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding:
                                    EdgeInsets.only(left: 20.0, right: 30.0),
                                child: Icon(
                                  FontAwesomeIcons.facebookF,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(
                                "Entrar com Facebook",
                                style: TextStyle(
                                    fontSize: 18.0, color: Colors.blue),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(top: 20.0, left: 10.0, right: 10.0),
                      child: GestureDetector(
                        child: Container(
                          height: 60.0,
                          decoration: BoxDecoration(
                            border:
                                Border.all(color: Colors.orange, width: 1.5),
                            borderRadius: BorderRadius.all(Radius.circular(
                                    10.0) //         <--- border radius here
                                ),
                          ),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding:
                                    EdgeInsets.only(left: 20.0, right: 30.0),
                                child: Icon(
                                  FontAwesomeIcons.google,
                                  color: Colors.grey,
                                ),
                              ),
                              Text(
                                "Entrar com Google",
                                style: TextStyle(
                                    fontSize: 18.0, color: Colors.orangeAccent),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 50.0,
                    )
                  ],
                ))));
  }
}
