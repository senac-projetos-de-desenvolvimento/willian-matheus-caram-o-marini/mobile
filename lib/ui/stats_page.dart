import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class StatsPage extends StatefulWidget {
  @override
  _StatsPageState createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        child: Padding(
      padding: EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(
                "Estatisticas",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
              ),
              Icon(
                Icons.pie_chart,
                size: 180.0,
              )
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Melhores Marcas",
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 20.0),
              ),
              Divider(),
              card("Pontos", "51 pontos", "7º Torneio", Icon(FontAwesomeIcons.basketballBall)),
              card("Rebotes", "30 rebotes", "4º Torneio", Icon(FontAwesomeIcons.grav)),
              card("Bloqueios", "11 bloqueios", "6º Torneio", Icon(FontAwesomeIcons.windowClose)),
              Container(
                height: 60.0,
                child: Card(
                  elevation: 2.0,
                  child: Padding(
                    padding: EdgeInsets.all(5.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        IconButton(icon: Icon(Icons.expand_more), iconSize: 30.0, onPressed: () {},),
                        Padding(
                          padding: EdgeInsets.only(left: 0.0),
                          child: Text(
                            "Ver Todas",
                            style: TextStyle(fontSize: 18.0),
                          ),
                        ),
                        //              child: Image.network(img),
                      ],
                    ),
                  ),
                ),
              ),
              Container(height: 20.0,)
            ],
          )
        ],
      ),
    ));
  }

  Widget card(String stats, String qtd, String torneio, Icon ic) {
    return Container(
      height: 80.0,
      child: Card(
        elevation: 2.0,
        child: Padding(
          padding: EdgeInsets.all(5.0),
          child: Row(
            children: <Widget>[
              IconButton(icon: ic, iconSize: 30.0, onPressed: () {},),
              Padding(
                padding: EdgeInsets.only(left: 5.0),
                child: Text(
                  stats,
                  style: TextStyle(fontSize: 18.0),
                ),
              ),
              //              child: Image.network(img),
              Container(
                  padding: EdgeInsets.only(top: 10.0, right: 1.0),
                  width: 150.0,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        qtd,
                        style: TextStyle(fontSize: 16.0),
                      ),
                      Text(
                        torneio,
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
