import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(
                top: 15.0, left: 10.0, right: 10.0, bottom: 15.0),
            child: Text(
              "Lembretes",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Table(
              border: TableBorder(
                horizontalInside: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
//                verticalInside: BorderSide(
//                  color: Colors.black,
//                  style: BorderStyle.solid,
//                  width: 1.0,
//                ),
                bottom: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                right: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                top: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                left: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
              ),
              children: [
                criarLinhaTable1("Data,Evento,Presença"),
                criarLinhaTable1("26/11/2019,3º Torneio,Confirmado"),
                criarLinhaTable1("19/12/2019,4º Torneio,À Confirmar"),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.only(
                top: 15.0, left: 10.0, right: 10.0, bottom: 15.0),
            child: Text(
              "Ultimas Atividades",
              style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(10.0),
            child: Table(
              border: TableBorder(
                horizontalInside: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                bottom: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                right: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                top: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
                left: BorderSide(
                    color: Colors.grey, style: BorderStyle.solid, width: 1.0),
              ),
              children: [
                criarLinhaTable1("Data,Evento,Opções"),
                criarLinhaTable2("10/09/2019", "3º Torneio"),
                criarLinhaTable2("08/09/2019", "Treino"),
                criarLinhaTable2("11/08/2019", "Treino"),
              ],
            ),
          )
        ],
      ),
    );
  }

  criarLinhaTable1(String listaNomes) {
    return TableRow(
      children: listaNomes.split(',').map((name) {
        return Container(
          alignment: Alignment.centerLeft,
          child: Text(
            name,
            style: TextStyle(fontSize: 15.0),
          ),
          padding: EdgeInsets.all(8.0),
        );
      }).toList(),
    );
  }

  criarLinhaTable2(String data, String evento) {
    return TableRow(children: [
      Container(
        alignment: Alignment.centerLeft,
        child: Text(
          data,
          style: TextStyle(fontSize: 15.0),
        ),
        padding: EdgeInsets.all(8.0),
      ),
      Container(
        alignment: Alignment.centerLeft,
        child: Text(
          evento,
          style: TextStyle(fontSize: 15.0),
        ),
        padding: EdgeInsets.all(8.0),
      ),
      Container(
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Icon(Icons.remove_red_eye),
            Padding(
              padding: EdgeInsets.only(left: 20.0),
            ),
            Icon(Icons.edit),
          ],
        ),
        padding: EdgeInsets.all(8.0),
      ),
    ]);
  }
}
