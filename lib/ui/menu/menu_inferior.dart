import 'package:control_game/ui/galeria_page.dart';
import 'package:control_game/ui/menu/menu_lateral.dart';
import 'package:control_game/ui/records_page.dart';
import 'package:control_game/ui/stats_page.dart';
import 'package:control_game/ui/treinos/treinos_home_page.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../home_page.dart';

class MenusPage extends StatefulWidget {
  @override
  _MenusPageState createState() => _MenusPageState();
}

class _MenusPageState extends State<MenusPage> {
  Widget bodyWidget;
  int index;

  final List<Widget> _children = [
    TreinoHomePage(),
    StatsPage(),
    HomePage(),
    RecordsPage(),
    GaleriaPage(),
  ];

  @override
  void initState() {
    super.initState();
    index = 2;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Icon(Icons.account_circle),
            Padding(
              padding: EdgeInsets.only(left: 10.0),
            ),
            Text("Willian")
          ],),
        centerTitle: true,
        actions: <Widget>[
          FloatingActionButton(
            onPressed: (){},
            child: IconButton(
              icon: Icon(Icons.add_circle_outline), onPressed: () {},
            ),
          ),
        ],
      ),
      bottomNavigationBar: MenuBottomBar(),
      drawer: MenuLateral(),
      body: _children[index],
    );
  }

  void _onBottomNavBarTab(int index) {
    setState(() {
      this.index = index;
    });
  }

  Widget MenuBottomBar(){
    return BottomNavigationBar(
      onTap: _onBottomNavBarTab,
      type: BottomNavigationBarType.fixed,
      backgroundColor: Colors.white70,
      currentIndex: index, // this will be set when a new tab is tapped
      items: [
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.male),
          title: Text('Treinos'),
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.database),
          title: Text('Estatisticas'),
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home')
        ),
        BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.trophy),
            title: Text('Recordes')
        ),
        BottomNavigationBarItem(
            icon: Icon(FontAwesomeIcons.photoVideo),
            title: Text('Galeria')
        )

      ],
    );
  }
}

