import 'package:control_game/ui/menu/menu_inferior.dart';
import 'package:control_game/ui/sign/sign_in_page.dart';
import 'package:control_game/ui/sign/sign_up_page.dart';
import 'package:flutter/material.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
//      home: MenusPage(),
      home: SignInPage(),
//      home: SignUpPage(),
    );
  }
}

